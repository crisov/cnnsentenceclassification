from sklearn.utils import shuffle

import pickle
import csv

import re
import regex
import html
from nltk.tokenize import wordpunct_tokenize
from nltk.corpus import stopwords

def read_TREC():
    data = {}

    def read(mode):
        x, y = [], []

        with open("data/TREC/TREC_" + mode + ".txt", "r", encoding="utf-8") as f:
            for line in f:
                if line[-1] == "\n":
                    line = line[:-1]
                y.append(line.split()[0].split(":")[0])
                x.append(line.split()[1:])

        x, y = shuffle(x, y)

        if mode == "train":
            dev_idx = len(x) // 10
            data["dev_x"], data["dev_y"] = x[:dev_idx], y[:dev_idx]
            data["train_x"], data["train_y"] = x[dev_idx:], y[dev_idx:]
        else:
            data["test_x"], data["test_y"] = x, y

    read("train")
    read("test")

    return data


def read_PrafullaEmo_train():
    data = {}
    x,y = [],[]
    def remove_stopwords(string):
        split_string = \
            [word for word in string.split()
             if word not in stopwords.words('english')]        
        return " ".join(split_string)
    
    def clean_str(string):  
        string = html.unescape(string)
        string = string.replace("\\n", " ")
        string = string.replace('un[#TRIGGERWORD#]', 'not [#TRIGGERWORD#]')
        string = string.replace("_NEG", "")
        string = string.replace("_NEGFIRST", "")
        string = re.sub(r"@[A-Za-z0-9_s(),!?\'\`]+", "", string) # removing any twitter handle mentions
    #     string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    #     string = re.sub(r"#", "", string)
        string = re.sub(r"\*", "", string)
        string = re.sub(r"\'s", "", string)
        string = re.sub(r"\'m", " am", string)
        string = re.sub(r"\'ve", " have", string)
        string = re.sub(r"n\'t", " not", string)
        string = re.sub(r"\'re", " are", string)
        string = re.sub(r"\'d", " would", string)
        string = re.sub(r"\'ll", " will", string)
        string = re.sub(r",", "", string)
        string = re.sub(r"!", " !", string)
        string = re.sub(r"\(", "", string)
        string = re.sub(r"\)", "", string)
        string = re.sub(r"\?", " ?", string)
        string = re.sub(r"\s{2,}", " ", string)
        #For emoji
        emojis = regex.findall(r'\p{So}\p{Sk}*',string)
        for emoji in set(emojis):
            string = string.replace(emoji, emoji+' ')
        return remove_stopwords(string.strip().lower())
    
    with open("data/Emo_WASSA/train.csv") as f:
        for line in f:
            line = line.strip()
            array = line.split('\t')
            if '[#TRIGGERWORD#]' not in array[1]:
                continue
            array[1] = array[1].replace('[#TRIGGERWORD#]','_TRIGGERWORD_')
            array[1] = wordpunct_tokenize(clean_str(array[1]))
            x.append(array[1])
            y.append(array[0])
    x,y = shuffle(x,y)
    #dev_idx = len(x) // 10
    #data["dev_x"], data["dev_y"] = x[:dev_idx], y[:dev_idx]
    data["train_x"], data["train_y"] = x, y

    x,y=[],[]

    with open("data/Emo_WASSA/trial.csv") as f, open('data/Emo_WASSA/trial.labels') as g:
        for line1, line2 in zip(f,g):
            line1 = line1.strip()
            array = line1.split('\t')
            if '[#TRIGGERWORD#]' not in array[1]:
                continue
            array[1] = array[1].replace('[#TRIGGERWORD#]','_TRIGGERWORD_')
            array[1] = wordpunct_tokenize(clean_str(array[1]))
            x.append(array[1])
            line2 = line2.strip()
            array = line2.split('\t')
            y.append(array[0])
    data["dev_x"], data["dev_y"] = x, y
    data['test_x'], data['test_y'] = x, y
        
    return data

def read_WASSA():
    data = {}
    labels = {"anger":0, "fear":1, "joy":2, "sadness":3}
    def remove_stopwords(string):
        split_string = \
            [word for word in string.split()
             if word not in stopwords.words('english')]
        
        return " ".join(split_string)
    def clean_str(string):  
        string = html.unescape(string)
        string = string.replace("\\n", " ")
        string = string.replace("_NEG", "")
        string = string.replace("_NEGFIRST", "")
        string = re.sub(r"@[A-Za-z0-9_s(),!?\'\`]+", "", string) # removing any twitter handle mentions
    #     string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    #     string = re.sub(r"#", "", string)
        string = re.sub(r"\*", "", string)
        string = re.sub(r"\'s", "", string)
        string = re.sub(r"\'m", " am", string)
        string = re.sub(r"\'ve", " have", string)
        string = re.sub(r"n\'t", " not", string)
        string = re.sub(r"\'re", " are", string)
        string = re.sub(r"\'d", " would", string)
        string = re.sub(r"\'ll", " will", string)
        string = re.sub(r",", "", string)
        string = re.sub(r"!", " !", string)
        string = re.sub(r"\(", "", string)
        string = re.sub(r"\)", "", string)
        string = re.sub(r"\?", " ?", string)
        string = re.sub(r"\s{2,}", " ", string)

        return remove_stopwords(string.strip().lower())

    def read(mode, emotion):
        x,y = [], []

        with open("data/WASSA2017/"+mode+"/"+emotion+"-ratings-0to1."+mode+"."+ \
            ("target.txt" if mode != "train" else "txt")) as f:
            for line in f:
                line = line.strip()
                array = line.split('\t')
                x.append(wordpunct_tokenize(clean_str(array[1])))
                y.append(array[-2])

            if (mode+'_x') in data:
                data[mode+'_x'].extend(x) 
            else:
                data[mode+'_x']=x
            
            if (mode+'_y') in data:
                data[mode+'_y'].extend(y) 
            else:
                data[mode+'_y']=y
    
    for mode in ['train','dev','test']:
        for emotion in labels:
            read(mode, emotion)
    return data




def read_MR():
    data = {}
    x, y = [], []

    with open("data/MR/rt-polarity.pos", "r", encoding="utf-8") as f:
        for line in f:
            if line[-1] == "\n":
                line = line[:-1]
            x.append(line.split())
            y.append(1)

    with open("data/MR/rt-polarity.neg", "r", encoding="utf-8") as f:
        for line in f:
            if line[-1] == "\n":
                line = line[:-1]
            x.append(line.split())
            y.append(0)

    x, y = shuffle(x, y)
    dev_idx = len(x) // 10 * 8
    test_idx = len(x) // 10 * 9

    data["train_x"], data["train_y"] = x[:dev_idx], y[:dev_idx]
    data["dev_x"], data["dev_y"] = x[dev_idx:test_idx], y[dev_idx:test_idx]
    data["test_x"], data["test_y"] = x[test_idx:], y[test_idx:]

    return data

def read_Yu():
    data = {}
    x, y = [], []
    def remove_stopwords(string):
        split_string = \
            [word for word in string.split()
             if word not in stopwords.words('english')]
        
        return " ".join(split_string)
    def clean_str(string):  
        string = html.unescape(string)
        string = string.replace("\\n", " ")
        string = string.replace("_NEG", "")
        string = string.replace("_NEGFIRST", "")
        string = re.sub(r"@[A-Za-z0-9_s(),!?\'\`]+", "", string) # removing any twitter handle mentions
    #     string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    #     string = re.sub(r"#", "", string)
        string = re.sub(r"\*", "", string)
        string = re.sub(r"\'s", "", string)
        string = re.sub(r"\'m", " am", string)
        string = re.sub(r"\'ve", " have", string)
        string = re.sub(r"n\'t", " not", string)
        string = re.sub(r"\'re", " are", string)
        string = re.sub(r"\'d", " would", string)
        string = re.sub(r"\'ll", " will", string)
        string = re.sub(r",", "", string)
        string = re.sub(r"!", " !", string)
        string = re.sub(r"\(", "", string)
        string = re.sub(r"\)", "", string)
        string = re.sub(r"\?", " ?", string)
        string = re.sub(r"\s{2,}", " ", string)

        return remove_stopwords(string.strip().lower())

    with open('data/PengYu/title_rel_assigned_Haley_20180607_0419PM - title_rel_assigned.csv', newline = '') as f:
        reader = csv.DictReader(f)
        for row in reader:
            y.append(row['CorrectResults'].rstrip('*'))
            line = wordpunct_tokenize(clean_str(row['title']))
            x.append(line)
        x, y = shuffle(x, y)
        dev_idx = len(y) // 10
        test_idx = len(y) // 10 + dev_idx

        data['train_x'], data['train_y'] = x[test_idx:], y[test_idx:]
        data['dev_x'], data['dev_y'] = x[:dev_idx], y[:dev_idx]
        data['test_x'], data['test_y'] = x[dev_idx:test_idx], y[dev_idx:test_idx]
        return data


def save_model(model, params):
    path = f"saved_models/{params['DATASET']}_{params['MODEL']}_{params['EPOCH']}.pkl"
    pickle.dump(model, open(path, "wb"))
    print(f"A model is saved successfully as {path}!")


def load_model(params):
    path = f"saved_models/{params['DATASET']}_{params['MODEL']}_{params['EPOCH']}.pkl"

    try:
        model = pickle.load(open(path, "rb"))
        print(f"Model in {path} loaded successfully!")

        return model
    except:
        print(f"No available model such as {path}.")
        exit()

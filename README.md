# Convolutional Neural Networks for Sentence Classification

This is the implementation of [Convolutional Neural Networks for Sentence Classification (Y.Kim, EMNLP 2014)](http://www.aclweb.org/anthology/D14-1181) on **Pytorch**.

Download pubmed_100.vec, pubmed_200.vec, pubmed_300.vec to the same project directory.

> python runSentClassification.py  --help

	usage: run.py [-h] [--mode MODE] [--model MODEL] [--dataset DATASET]
				  [--save_model] [--early_stopping] [--epoch EPOCH]
				  [--learning_rate LEARNING_RATE] [--gpu GPU] [--dimension DIMENSION]

	-----[CNN-classifier]-----

	optional arguments:
	  -h, --help            show this help message and exit
	  --mode MODE           train: train (with test) a model / test: test saved
							models
	  --model MODEL         available models: rand, static, non-static,
							multichannel
	  --dataset DATASET     available datasets: Yu
	  --save_model          whether saving model or not
	  --early_stopping      whether to apply early stopping
	  --epoch EPOCH         number of max epoch
	  --learning_rate LEARNING_RATE
							learning rate
	  --gpu GPU             the number of gpu to be used
      --dimension DIM.      available dimensionality: 100, 200, 300, default 300  
      --cpu CPU				whether to use cpu or not 

Example: python runSentClassification.py --mode train --model multichannel --dataset Yu --dimension 100 --cpu
  

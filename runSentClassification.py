from model import CNN
import utils

from torch.autograd import Variable
import torch
import torch.optim as optim
import torch.nn as nn

from sklearn.utils import shuffle
from gensim.models.keyedvectors import KeyedVectors
import numpy as np
import argparse
import copy

#confusion Matrix, precision recall fscore
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_fscore_support
from prettytable import PrettyTable


def train(data, params):
    if params["MODEL"] != "rand":
        # load word2vec
        print("loading word2vec...")
        if params['WORD_DIM'] == 300:
            word_vectors = KeyedVectors.load_word2vec_format("pubmed_300.vec", binary=False)
        elif params['WORD_DIM'] == 200:
            word_vectors = KeyedVectors.load_word2vec_format("pubmed_200.vec", binary=False)
        else:
            word_vectors = KeyedVectors.load_word2vec_format("pubmed_100.vec", binary=False)

        wv_matrix = []
        for i in range(len(data["vocab"])):
            word = data["idx_to_word"][i]
            if word in word_vectors.vocab:
                wv_matrix.append(word_vectors.word_vec(word))
            else:
                wv_matrix.append(np.random.uniform(-0.01, 0.01, params['WORD_DIM']).astype("float32"))

        # one for UNK and one for zero padding
        wv_matrix.append(np.random.uniform(-0.01, 0.01, params['WORD_DIM']).astype("float32"))
        wv_matrix.append(np.zeros(params['WORD_DIM']).astype("float32"))
        wv_matrix = np.array(wv_matrix)
        params["WV_MATRIX"] = wv_matrix
    if params["CPU"]:
        model = CNN(**params)
    else:
        model = CNN(**params).cuda(params["GPU"])

    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = optim.Adadelta(parameters, params["LEARNING_RATE"])
    criterion = nn.CrossEntropyLoss()

    pre_dev_acc = 0
    max_dev_acc = 0
    max_test_acc = 0
    for e in range(params["EPOCH"]):
        data["train_x"], data["train_y"] = shuffle(data["train_x"], data["train_y"])

        for i in range(0, len(data["train_x"]), params["BATCH_SIZE"]):
            batch_range = min(params["BATCH_SIZE"], len(data["train_x"]) - i)

            batch_x = [[data["word_to_idx"][w] for w in sent] +
                       [params["VOCAB_SIZE"] + 1] * (params["MAX_SENT_LEN"] - len(sent))
                       for sent in data["train_x"][i:i + batch_range]]
            batch_y = [data["classes"].index(c) for c in data["train_y"][i:i + batch_range]]
            if params['CPU']:
                batch_x = Variable(torch.LongTensor(batch_x))
                batch_y = Variable(torch.LongTensor(batch_y))
            else:
                batch_x = Variable(torch.LongTensor(batch_x)).cuda(params["GPU"])
                batch_y = Variable(torch.LongTensor(batch_y)).cuda(params["GPU"])

            optimizer.zero_grad()
            model.train()
            pred = model(batch_x)
            loss = criterion(pred, batch_y)
            loss.backward()
            nn.utils.clip_grad_norm(parameters, max_norm=params["NORM_LIMIT"])
            optimizer.step()
            del loss
            del pred
            torch.cuda.empty_cache()

        dev_acc, y_dev_pred, y_dev_true = test(data, model, params, mode="dev")
        test_acc, y_test_pred, y_test_true = test(data, model, params)
        print("epoch:", e + 1, "/ dev_acc:", dev_acc, "/ test_acc:", test_acc)
        
        if params["EARLY_STOPPING"] and dev_acc <= pre_dev_acc:
            print("early stopping by dev_acc!")
            break
        else:
            pre_dev_acc = dev_acc

        if dev_acc > max_dev_acc:
            max_dev_acc = dev_acc
            max_test_acc = test_acc
            max_y_dev_pred = y_dev_pred
            max_y_dev_true = y_dev_true
            max_y_test_pred = y_test_pred
            max_y_test_true = y_test_true
            best_model = copy.deepcopy(model)

    print("max dev acc:", max_dev_acc, "test acc:", max_test_acc)
    analysis(data, max_y_dev_pred, max_y_dev_true, mode='dev')
    analysis(data, max_y_test_pred, max_y_test_true, mode='test')
    return best_model


def test(data, model, params, mode="test"):
    model.eval()

    if mode == "dev":
        x, y = data["dev_x"], data["dev_y"]
    elif mode == "test":
        x, y = data["test_x"], data["test_y"]

    x = [[data["word_to_idx"][w] if w in data["vocab"] else params["VOCAB_SIZE"] for w in sent] +
         [params["VOCAB_SIZE"] + 1] * (params["MAX_SENT_LEN"] - len(sent))
         for sent in x]

    max_index = len(x)
    a,b = 0,min(2000,max_index)
    acc_list = []
    weights = []
    y_true = [data["classes"].index(c) for c in y]
    y_pred = []
    print('='*20 + 'Now in Loop' + '='*20)
    while True:
        if params['CPU']:
            x_tmp = Variable(torch.LongTensor(x[a:b]))
        else:
            x_tmp = Variable(torch.LongTensor(x[a:b])).cuda(params["GPU"])
        y_tmp = [data["classes"].index(c) for c in y[a:b]]
        pred = np.argmax(model(x_tmp).cpu().data.numpy(), axis=1)
        y_pred.extend(pred)
        acc = sum([1 if p == y else 0 for p, y in zip(pred, y_tmp)]) / len(pred)
        print("Currently the accuracy of dataset index from {0} to {1} is {2}".format(a,b,acc))
        acc_list.append(acc)
        weights.append(b-a)
        a = b
        b = b + 2000
        del x_tmp
        del y_tmp
        del pred
        del acc
        if not params['CPU']:
            torch.cuda.empty_cache()
        
        if b >= max_index+2000:
            break 
        elif b >= max_index:
            b = max_index

    acc = np.average(acc_list, weights=weights)
    '''
    if mode=='test':
        with open("data/Emo_WASSA/trial.csv") as f:
            for line, y_pred_, y_true_ in zip(f.readlines(), y_pred, y_true):
                if y_pred_ != y_true_:
                    print(line, data['classes'][y_true_], data['classes'][y_pred_])
    '''
    return acc, y_pred, y_true

def analysis(data, y_pred, y_true, mode):
    y = data[mode+'_y']
    if y != y_true:
        print("="*20+'WARNNING'+"="*20)
        print(data['classes'])
        print(set(y_true)) 
        print(set(y_pred))
        print("The true labels is defferent from labels extracted from original data")
    
    labels = list(set(y_true))
    print('='*20+mode+'='*20)
    print("labels: "+', '.join(str(label) for label in labels))
    
    #confusion matrix
    CM = confusion_matrix(y_true, y_pred, labels=labels)
    norm_CM = CM.astype('float') / CM.sum(axis=1)[:, np.newaxis]
    precision, recall, fscore, support = precision_recall_fscore_support(y_true, y_pred, labels=labels)
    print('confusion_matrix: ')
    print(CM)
    print('Normalized Confusion Matrix: ')
    print(norm_CM)
    table = PrettyTable()
    table.add_column('label',labels)
    table.add_column('precision', precision)
    table.add_column('recall', recall) 
    table.add_column('fscore',fscore)
    table.add_column('support', support)
    print(table)







def main():
    parser = argparse.ArgumentParser(description="-----[CNN-classifier]-----")
    parser.add_argument("--mode", default="train", help="train: train (with test) a model / test: test saved models")
    parser.add_argument("--model", default="rand", help="available models: rand, static, non-static, multichannel")
    parser.add_argument("--dataset", default="TREC", help="available datasets: MR, TREC")
    parser.add_argument("--save_model", default=False, action='store_true', help="whether saving model or not")
    parser.add_argument("--early_stopping", default=False, action='store_true', help="whether to apply early stopping")
    parser.add_argument("--epoch", default=100, type=int, help="number of max epoch")
    parser.add_argument("--learning_rate", default=1.0, type=float, help="learning rate")
    parser.add_argument("--gpu", default=-1, type=int, help="the number of gpu to be used")
    parser.add_argument("--dimension", default=300, type=int, help="the word2vec dimension, default is 300")
    parser.add_argument("--cpu", default = False, action='store_true', help='whether to use cpu or not')

    options = parser.parse_args()
    data = getattr(utils, f"read_{options.dataset}")()

    data["vocab"] = sorted(list(set([w for sent in data["train_x"] + data["dev_x"] + data["test_x"] for w in sent])))
    data["classes"] = sorted(list(set(data["train_y"])))
    data["word_to_idx"] = {w: i for i, w in enumerate(data["vocab"])}
    data["idx_to_word"] = {i: w for i, w in enumerate(data["vocab"])}

    params = {
        "MODEL": options.model,
        "DATASET": options.dataset,
        "SAVE_MODEL": options.save_model,
        "EARLY_STOPPING": options.early_stopping,
        "EPOCH": options.epoch,
        "LEARNING_RATE": options.learning_rate,
        "MAX_SENT_LEN": max([len(sent) for sent in data["train_x"] + data["dev_x"] + data["test_x"]]),
        "BATCH_SIZE": 50,
        "WORD_DIM": options.dimension,
        "VOCAB_SIZE": len(data["vocab"]),
        "CLASS_SIZE": len(data["classes"]),
        "FILTERS": [3, 4, 5],
        "FILTER_NUM": [100, 100, 100],
        "DROPOUT_PROB": 0.5,
        "NORM_LIMIT": 3,
        "GPU": options.gpu,
        "CPU": options.cpu
    }

    print("=" * 20 + "INFORMATION" + "=" * 20)
    print("MODEL:", params["MODEL"])
    print("DATASET:", params["DATASET"])
    print("VOCAB_SIZE:", params["VOCAB_SIZE"])
    print("EPOCH:", params["EPOCH"])
    print("LEARNING_RATE:", params["LEARNING_RATE"])
    print("EARLY_STOPPING:", params["EARLY_STOPPING"])
    print("SAVE_MODEL:", params["SAVE_MODEL"])
    print("=" * 20 + "INFORMATION" + "=" * 20)

    if options.mode == "train":
        print("=" * 20 + "TRAINING STARTED" + "=" * 20)
        model = train(data, params)
        if params["SAVE_MODEL"]:
            utils.save_model(model, params)
        print("=" * 20 + "TRAINING FINISHED" + "=" * 20)
    else:
        if options.cpu:
            model = utils.load_model(params)
        else:
            model = utils.load_model(params).cuda(params["GPU"])

        test_acc, y_pred, y_true = test(data, model, params)
        analysis(data, y_pred, y_true,mode='test')
        print("test acc:", test_acc)
        dev_acc, y_pred, y_true = test(data, model, params, mode='dev')
        analysis(data, y_pred, y_true, mode='dev')
        print("dev acc:", dev_acc)


if __name__ == "__main__":
    main()
